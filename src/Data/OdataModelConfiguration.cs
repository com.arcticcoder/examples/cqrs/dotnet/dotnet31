﻿using Com.ArcticCoder.Examples.CQRS.Data.Models;
using Microsoft.AspNet.OData.Builder;
using Microsoft.AspNetCore.Mvc;

namespace Com.ArcticCoder.Examples.CQRS.Data
{
    public class OdataModelConfiguration : IModelConfiguration
    {
        public void Apply(ODataModelBuilder builder, ApiVersion apiVersion)
        {
            builder.EntitySet<Person>("Persons");
        }
    }
}
