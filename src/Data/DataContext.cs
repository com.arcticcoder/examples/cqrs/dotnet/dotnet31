using Com.ArcticCoder.Examples.CQRS.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace Com.ArcticCoder.Examples.CQRS.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options) { }

        public DbSet<Person> Persons { get; set; }
    }
}