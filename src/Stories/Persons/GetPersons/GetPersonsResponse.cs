﻿using System.Collections.Generic;
using Com.ArcticCoder.Examples.CQRS.Data.Models;

namespace Com.ArcticCoder.Examples.CQRS.Stories.Persons.GetPersons
{
    public class GetPersonsResponse
    {
        public IEnumerable<Person> Persons { get; set; }
    }
}
