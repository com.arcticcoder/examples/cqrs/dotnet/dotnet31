﻿using Com.ArcticCoder.Examples.CQRS.Domain.Abstraction;
using MediatR;

namespace Com.ArcticCoder.Examples.CQRS.Stories.Persons.GetPersons
{
    public class GetPersonsRequest : IRequest<IServiceResult<GetPersonsResponse>>
    {
        
    }
}
