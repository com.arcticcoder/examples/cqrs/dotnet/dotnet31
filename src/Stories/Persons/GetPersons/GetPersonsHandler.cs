﻿using System.Threading;
using System.Threading.Tasks;
using Com.ArcticCoder.Examples.CQRS.Data;
using Com.ArcticCoder.Examples.CQRS.Domain;
using Com.ArcticCoder.Examples.CQRS.Domain.Abstraction;
using Com.ArcticCoder.Examples.CQRS.Resources.Texts;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Com.ArcticCoder.Examples.CQRS.Stories.Persons.GetPersons
{
    public class GetPersonsHandler : IRequestHandler<GetPersonsRequest, IServiceResult<GetPersonsResponse>>
    {
        private readonly DataContext _dataContext;
        private readonly ICommonTexts _commonTexts;

        public GetPersonsHandler(DataContext dataContext, ICommonTexts commonTexts)
        {
            _dataContext = dataContext;
            _commonTexts = commonTexts;
        }

        public async Task<IServiceResult<GetPersonsResponse>> Handle(GetPersonsRequest request, CancellationToken cancellationToken)
        {
            var persons = await _dataContext.Persons.ToListAsync();

            GetPersonsResponse response = new GetPersonsResponse
            {
                Persons = persons
            };            

            return ServiceResult<GetPersonsResponse>.Failed(new ServiceError() { Key = "", Value = _commonTexts.ErrorGeneric });

            return ServiceResult<GetPersonsResponse>.Success(response);
        }        
    }
}
