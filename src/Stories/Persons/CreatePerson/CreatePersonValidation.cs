﻿using FluentValidation;

namespace Com.ArcticCoder.Examples.CQRS.Stories.Persons.CreatePerson
{
    public class CreatePersonValidation : AbstractValidator<CreatePersonRequest>
    {
        public CreatePersonValidation()
        {
            RuleFor(x => x.Age)                
                .NotNull().WithMessage("Ålder måste anges")
                .LessThan(CreatePersonRequest.MAX_AGE).WithMessage("Ålder måste lägre än {MAX_AGE} år".Replace("{MAX_AGE}", $"{CreatePersonRequest.MAX_AGE}"))
                .GreaterThan(CreatePersonRequest.MIN_AGE).WithMessage("Ålder måste högre än {MIN_AGE} år".Replace("{MIN_AGE}", $"{CreatePersonRequest.MIN_AGE}"));

            RuleFor(x => x.FirstName)
                .NotEmpty().WithMessage("Förnamn måste anges")
                .NotNull().WithMessage("Förnamn måste anges")
                .MaximumLength(CreatePersonRequest.FIRST_NAME_MAX_LENGTH).WithMessage("Förnamn får inte överstiga {FIRST_NAME_MAX_LENGTH} tecken".Replace("{FIRST_NAME_MAX_LENGTH}", $"{CreatePersonRequest.FIRST_NAME_MAX_LENGTH}"));
        }
    }
}
