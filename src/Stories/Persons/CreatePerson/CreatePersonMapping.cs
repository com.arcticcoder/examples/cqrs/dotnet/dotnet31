﻿using AutoMapper;
using Com.ArcticCoder.Examples.CQRS.Data.Models;

namespace Com.ArcticCoder.Examples.CQRS.Stories.Persons.CreatePerson
{
    public class CreatePersonMapping : Profile
    {
        public CreatePersonMapping()
        {
            // source, dest
            CreateMap<Person, CreatedPerson>()
                .ForMember(dest => dest.Age, opt => opt.MapFrom(src => src.Age))
                .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id));


            CreateMap<CreatePersonRequest, Person>()
                .ForMember(dest => dest.Age, opt => opt.MapFrom(src => src.Age))
                .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
                .ForAllOtherMembers(opt => opt.Ignore());
        }
    }
}
