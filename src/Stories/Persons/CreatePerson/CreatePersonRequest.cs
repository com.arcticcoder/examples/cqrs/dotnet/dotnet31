using Com.ArcticCoder.Examples.CQRS.Domain.Abstraction;
using MediatR;

namespace Com.ArcticCoder.Examples.CQRS.Stories.Persons.CreatePerson
{
    public class CreatePersonRequest : IRequest<IServiceResult<CreatedPerson>>
    {
        public const int MIN_AGE = 0;
        public const int MAX_AGE = 120;
        public const int FIRST_NAME_MAX_LENGTH = 10;
        public string FirstName { get; set; }
        public int Age { get; set; }
    }
}