﻿using System;

namespace Com.ArcticCoder.Examples.CQRS.Stories.Persons.CreatePerson
{
    public class CreatedPerson
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public int Age { get; set; }
    }
}