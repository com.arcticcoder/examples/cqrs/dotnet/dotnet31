using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Com.ArcticCoder.Examples.CQRS.Data;
using Com.ArcticCoder.Examples.CQRS.Data.Models;
using Com.ArcticCoder.Examples.CQRS.Domain;
using Com.ArcticCoder.Examples.CQRS.Domain.Abstraction;
using MediatR;

namespace Com.ArcticCoder.Examples.CQRS.Stories.Persons.CreatePerson
{
    public class CreatePersonHandler : IRequestHandler<CreatePersonRequest, IServiceResult<CreatedPerson>>
    {
        private readonly DataContext ctx;
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;

        public CreatePersonHandler(DataContext ctx, IMediator mediator, IMapper mapper)
        {
            this.ctx = ctx;
            _mediator = mediator;
            _mapper = mapper;
        }

        // Implements and encapsulates the real business logic
        public async Task<IServiceResult<CreatedPerson>> Handle(CreatePersonRequest request, CancellationToken cancellationToken)
        {
            Person person = _mapper.Map<Person>(request);
            ctx.Add(person);

            int response = await ctx.SaveChangesAsync();
            if(response == 0)
            {
                throw new ApplicationException();
            }

            // Publish a PersonCreatedEvent to all listenes, i.e. those that implements "INotificationHandler<CreatePersonEvent>"
            await _mediator.Publish(new CreatePersonEvent(person.Id), cancellationToken);

            var mapped = _mapper.Map<CreatedPerson>(person);
            return ServiceResult<CreatedPerson>.Success(mapped);
        }
    }
}