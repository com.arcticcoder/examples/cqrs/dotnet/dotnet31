﻿using System;
using MediatR;

namespace Com.ArcticCoder.Examples.CQRS.Stories.Persons.CreatePerson
{
    public class CreatePersonEvent : INotification
    {
        public CreatePersonEvent(Guid personId)
        {
            PersonId = personId;
        }

        public Guid PersonId { get; }
    }
}
