﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Com.ArcticCoder.Examples.CQRS.Data;
using Com.ArcticCoder.Examples.CQRS.Stories.Persons.CreatePerson;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Com.ArcticCoder.Examples.CQRS.Stories
{
    /// <summary>
    /// This handler listens for INotificationHandler<CreatePersonEvent> and handles them.
    /// </summary>
    public class ExamplePersonCreatedHandler : INotificationHandler<CreatePersonEvent>
    {
        private readonly DataContext _dataContext;
        private readonly ILogger<ExamplePersonCreatedHandler> _logger;

        public ExamplePersonCreatedHandler(DataContext dataContext, ILogger<ExamplePersonCreatedHandler> logger)
        {
            _dataContext = dataContext;
            _logger = logger;
        }

        public async Task Handle(CreatePersonEvent notification, CancellationToken cancellationToken)
        {
            var person = await _dataContext.Persons.Where(p => p.Id == notification.PersonId).FirstOrDefaultAsync();

            if (person == null)
            {
                //TODO: Handle Next business logic if person is not found
                _logger.LogWarning("Person is not found by personid from publisher");
            }
            else
            {
                _logger.LogInformation($"Person has found by personid: {notification.PersonId} from publisher");
            }
        }
    }
}
