﻿using System.Threading;
using System.Threading.Tasks;
using Com.ArcticCoder.Examples.CQRS.Domain;
using Com.ArcticCoder.Examples.CQRS.Domain.Abstraction;
using Com.ArcticCoder.Examples.CQRS.Resources.Texts;
using MediatR;

namespace Com.ArcticCoder.Examples.CQRS.Stories.Errors.GetErrorExample
{
    public class GetErrorExampleHandler : IRequestHandler<GetErrorExampleRequest, IServiceResult<GetErrorExampleResponse>>
    {
        private readonly ICommonTexts _commonTexts;

        public GetErrorExampleHandler(ICommonTexts commonTexts)
        {
            _commonTexts = commonTexts;
        }

        public async Task<IServiceResult<GetErrorExampleResponse>> Handle(GetErrorExampleRequest request, CancellationToken cancellationToken)
        {
            return ServiceResult<GetErrorExampleResponse>.Failed(new ServiceError() { Key = "", Value = _commonTexts.ErrorGeneric });
        }        
    }
}
