﻿using Com.ArcticCoder.Examples.CQRS.Domain.Abstraction;
using MediatR;

namespace Com.ArcticCoder.Examples.CQRS.Stories.Errors.GetErrorExample
{
    public class GetErrorExampleRequest : IRequest<IServiceResult<GetErrorExampleResponse>>
    {
        
    }
}
