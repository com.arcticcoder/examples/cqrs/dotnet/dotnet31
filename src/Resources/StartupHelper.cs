﻿using System;
using System.Collections.Generic;
using Com.ArcticCoder.Examples.CQRS.Resources.Texts;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Localization;
using Microsoft.Extensions.DependencyInjection;

namespace Com.ArcticCoder.Examples.CQRS.Resources
{
    public static class StartupHelper
    {
        public static IServiceCollection RegisterLanguages(this IServiceCollection services, string defaultCulture = "sv-SE", params string[] supportedCultures)
        {
            string[] suportedLanguages = GetSupportedLanguages(defaultCulture, supportedCultures);

            // On every request the list of RequestCultureProvider in the RequestLocalizationOptions is enumerated and the first provider that can successfully determine the request culture is used.The default providers come from the RequestLocalizationOptions class:
            // QueryStringRequestCultureProvider
            // CookieRequestCultureProvider
            // AcceptLanguageHeaderRequestCultureProvider
            services.Configure<AcceptLanguageHeaderRequestCultureProvider>(options =>
            {
                options.Options.AddSupportedCultures(suportedLanguages);
                options.Options.AddSupportedUICultures(suportedLanguages);
                options.Options.DefaultRequestCulture = new RequestCulture(culture: defaultCulture, uiCulture: defaultCulture);
                options.Options.SetDefaultCulture(defaultCulture);
            });

            services.AddSingleton<ICommonTexts, CommonTexts>();
            services.AddLocalization();

            return services;
        }

        public static IApplicationBuilder SetRequestLanguage(this IApplicationBuilder app, string defaultCulture = "sv-SE", params string[] supportedCultures)
        {
            string[] suportedLanguages = GetSupportedLanguages(defaultCulture, supportedCultures);
            
            var localizationOptions = new RequestLocalizationOptions()
                .SetDefaultCulture(defaultCulture)
                .AddSupportedCultures(suportedLanguages)
                .AddSupportedUICultures(suportedLanguages);

            app.UseRequestLocalization(localizationOptions);

            return app;
        }

        private static string[] GetSupportedLanguages(string defaultCulture, params string[] supportedCultures)
        {
            List<string> supportedCultureList = new List<string>();
            bool addDefaultCulture = true;
            defaultCulture = defaultCulture.Trim();

            if (string.IsNullOrEmpty(defaultCulture))
                throw new ArgumentNullException("Default culture cannot be null or empty");

            // If the default culture is not detected in the supported cultures array it is added.
            if (supportedCultures != null && supportedCultures?.Length > 0)
            {
                for (int i = 0; i < supportedCultures.Length; i++)
                {
                    supportedCultures[i] = supportedCultures[i].Trim();

                    // Check that the suopported culture is not null or empty
                    if (string.IsNullOrEmpty(supportedCultures[i]))
                        throw new ArgumentNullException($"SupportedCultures[{i}] cannot be null or empty");

                    if (supportedCultures[i] == defaultCulture)
                    {
                        addDefaultCulture = false;
                    }

                    supportedCultureList.Add(supportedCultures[i]);
                }
            }

            if (addDefaultCulture)
                supportedCultureList.Add(defaultCulture);

            return supportedCultureList.ToArray();
        }
    }
}
