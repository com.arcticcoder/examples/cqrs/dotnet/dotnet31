﻿using Microsoft.Extensions.Localization;

namespace Com.ArcticCoder.Examples.CQRS.Resources.Texts
{
    public interface ICommonTexts
    {
        string ErrorNoRecordsSaved { get; }
        string ErrorConcurrency { get; }
        string ErrorGeneric { get; }
    }

    public class CommonTexts : ICommonTexts
    {
        private readonly IStringLocalizer<CommonTexts> _localizer;

        public CommonTexts(IStringLocalizer<CommonTexts> localizer) => _localizer = localizer;

        public string ErrorNoRecordsSaved => GetString(nameof(ErrorNoRecordsSaved));
        public string ErrorConcurrency => GetString(nameof(ErrorConcurrency));
        public string ErrorGeneric => GetString(nameof(ErrorGeneric));

        private string GetString(string name) => _localizer[name];
    }
}
