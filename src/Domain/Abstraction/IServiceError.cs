﻿namespace Com.ArcticCoder.Examples.CQRS.Domain.Abstraction
{
    public interface IServiceError
    {
        //
        // Summary:
        //     Gets or sets the key for this error.
        public string Key { get; }
        //
        // Summary:
        //     Gets or sets the description for this error.
        public string Value { get; }
    }
}
