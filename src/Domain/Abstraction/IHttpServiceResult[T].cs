﻿using System.Net;

namespace Com.ArcticCoder.Examples.CQRS.Domain.Abstraction
{
    public interface IHttpServiceResult<T> : IServiceResult<T>
    {
        HttpStatusCode StatusCode { get; }
    }
}
