﻿using System.Collections.Generic;

namespace Com.ArcticCoder.Examples.CQRS.Domain.Abstraction
{
    public interface IServiceResult<T>
    {
        bool Succeeded { get; }
        IEnumerable<IServiceError> Errors { get; }
        bool HasErrors { get; }
        T Value { get; }
    }
}
