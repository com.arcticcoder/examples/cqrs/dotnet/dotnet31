﻿using System.Net;
using Com.ArcticCoder.Examples.CQRS.Domain.Abstraction;

namespace Com.ArcticCoder.Examples.CQRS.Domain
{
    public class HttpServiceResult<T> : ServiceResult<T>, IHttpServiceResult<T>
    {
        public HttpStatusCode StatusCode { get; protected set; }

        //
        // Summary:
        //     Returns an ServiceResult indicating a successful operation.
        //
        // Returns:
        //     An ServiceResult indicating a successful operation.
        public static HttpServiceResult<T> Success(HttpStatusCode statusCode, T value)
        {
            return new HttpServiceResult<T> { Succeeded = true, Value = value, StatusCode = statusCode };
        }

        //
        // Summary:
        //     Creates an ServiceResult indicating a failed operation, 
        //     with a list of errors if applicable.
        //
        // Parameters:
        //   errors:
        //     An optional array of ServiceErrors which caused
        //     the operation to fail.
        //
        // Returns:
        //     An ServiceResult indicating a failed operation, 
        //     with a list of errors if applicable.
        public static HttpServiceResult<T> Failed(HttpStatusCode statusCode, params IServiceError[] errors)
        {
            var result = new HttpServiceResult<T> { Succeeded = false, Value = default(T), StatusCode = statusCode };
            if (errors != null)
            {
                result.AddErrors(errors);
            }
            return result;
        }
    }
}
