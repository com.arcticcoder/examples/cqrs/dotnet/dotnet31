﻿using Com.ArcticCoder.Examples.CQRS.Domain.Abstraction;

namespace Com.ArcticCoder.Examples.CQRS.Domain
{
    public class ServiceError : IServiceError
    {
        //
        // Summary:
        //     Gets or sets the key for this error.
        public string Key { get; set; }
        //
        // Summary:
        //     Gets or sets the description for this error.
        public string Value { get; set; }
    }
}
