﻿using System.Collections.Generic;
using System.Linq;
using Com.ArcticCoder.Examples.CQRS.Domain.Abstraction;

namespace Com.ArcticCoder.Examples.CQRS.Domain
{
    public class ServiceResult<T> : IServiceResult<T>
    {

        public T Value { get; protected set; }

        public bool HasErrors
        {
            get
            {
                if (_errors?.Count > 0)
                    return true;

                return false;
            }
        }

        private List<IServiceError> _errors = new List<IServiceError>();

        //
        // Summary:
        //     Returns an ServiceResult indicating a successful operation.
        //
        // Returns:
        //     An ServiceResult indicating a successful operation.
        public static ServiceResult<T> Success(T value)
        {
            return new ServiceResult<T> { Succeeded = true, Value = value };
        }

        //
        // Summary:
        //     Flag indicating whether if the operation succeeded or not.
        public bool Succeeded { get; protected set; }

        //
        // Summary:
        //     An System.Collections.Generic.IEnumerable`1 of ServiceResult
        //     containing an errors that occurred during the operation.
        public IEnumerable<IServiceError> Errors => _errors;

        //
        // Summary:
        //     Creates an ServiceResult indicating a failed operation, 
        //     with a list of errors if applicable.
        //
        // Parameters:
        //   errors:
        //     An optional array of ServiceErrors which caused
        //     the operation to fail.
        //
        // Returns:
        //     An ServiceResult indicating a failed operation, 
        //     with a list of errors if applicable.
        public static ServiceResult<T> Failed(params ServiceError[] errors)
        {
            var result = new ServiceResult<T> { Succeeded = false, Value = default(T) };
            if (errors != null)
            {
                result._errors.AddRange(errors);
            }
            return result;
        }

        public void AddError(string key, string value) => _errors.Add(new ServiceError { Key = key, Value = value });

        public void AddErrors(params IServiceError[] errors)
        {
            if (errors != null)
            {
                _errors.AddRange(errors);
            }
        }

        //
        // Summary:
        //     Converts the value of the current ServiceResult
        //     object to its equivalent string representation.
        //
        // Returns:
        //     A string representation of the current ServiceResult
        //     object.
        //
        // Remarks:
        //     If the operation was successful the ToString() will return "Succeeded" otherwise
        //     it returned "Failed : " followed by a comma delimited list of error codes from
        //     its ServiceResult.Errors collection, if any.
        public override string ToString()
        {
            return Succeeded ?
                "Succeeded" :
                string.Format("{0} : {1}", "Failed", string.Join(",", Errors.Select(x => nameof(x.Key)).ToList()));
        }
    }
}
