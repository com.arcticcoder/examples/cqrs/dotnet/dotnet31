﻿using System.Threading.Tasks;
using Com.ArcticCoder.Examples.CQRS.Domain.Abstraction;
using Com.ArcticCoder.Examples.CQRS.Stories.Persons.GetPersons;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Com.ArcticCoder.Examples.CQRS.API.Controllers.Persons
{
    [ApiController]
    [Route("api/persons")]
    [Produces("application/json")]
    public partial class PersonsController : ApiControllerBase
    {
        public PersonsController(IMediator mediator) : base(mediator)
        {

        }

        /// <summary>
        /// Create new person
        /// </summary>
        /// <param name="command">Info of customer</param>
        /// <returns></returns>
        [HttpGet("")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<ActionResult> GetPersons([FromQuery]GetPersonsRequest query)
        {
            IServiceResult<GetPersonsResponse> response = await QueryAsync(query);

            if (!response.Succeeded)
            {
                if (response.HasErrors)
                {
                    foreach (var item in response.Errors)
                    {
                        ModelState.AddModelError(item.Key, item.Value);
                    }

                    return ValidationProblem(ModelState);
                }

                return BadRequest();
            }

            return Ok(response);
        }
    }
}
