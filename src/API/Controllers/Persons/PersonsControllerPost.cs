﻿using System.Threading.Tasks;
using Com.ArcticCoder.Examples.CQRS.Domain.Abstraction;
using Com.ArcticCoder.Examples.CQRS.Stories.Persons.CreatePerson;
using Microsoft.AspNetCore.Mvc;

namespace Com.ArcticCoder.Examples.CQRS.API.Controllers.Persons
{
    public partial class PersonsController
    {
        [HttpPost("")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<ActionResult<CreatedPerson>> CreatePerson(CreatePersonRequest command)
        {
            // let MediatR dispatch the incoming command
            IServiceResult<CreatedPerson> response = await CommandAsync(command);

            if (!response.Succeeded)
            {
                if (response.HasErrors)
                {
                    foreach (var item in response.Errors)
                    {
                        ModelState.AddModelError(item.Key, item.Value);
                    }

                    return ValidationProblem(ModelState);
                }

                return BadRequest();
            }

            return Ok(response.Value);
        }
    }
}
