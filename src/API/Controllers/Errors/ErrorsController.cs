﻿using System.Threading.Tasks;
using Com.ArcticCoder.Examples.CQRS.Domain.Abstraction;
using Com.ArcticCoder.Examples.CQRS.Stories.Errors.GetErrorExample;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Com.ArcticCoder.Examples.CQRS.API.Controllers.Errors
{

    [Produces("application/json")]
    [Route("api/errors")]
    [ApiController]
    public class ErrorsController : ApiControllerBase
    {
        public ErrorsController(IMediator mediator) : base(mediator)
        {
                
        }

        /// <summary>
        /// This should return a resource error which can be set based on the accept-language in the header.
        /// </summary>
        /// <param name="command">Info of customer</param>
        /// <returns></returns>
        [HttpGet("")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<ActionResult> GetError([FromQuery] GetErrorExampleRequest query)
        {
            IServiceResult<GetErrorExampleResponse> response = await QueryAsync(query);

            if (!response.Succeeded)
            {
                if (response.HasErrors)
                {
                    foreach (var item in response.Errors)
                    {
                        ModelState.AddModelError(item.Key, item.Value);
                    }

                    return ValidationProblem(ModelState);
                }

                return BadRequest();
            }

            return Ok(response);
        }
    }
}
